using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace LambdaS3;

public class Function
{
    public async Task FunctionHandler(S3Event inputEvent, ILambdaContext context)
    {
        foreach (var record in inputEvent.Records)
        {
            var emailSettings = new EmailSettings();
            var s3Request = new GetObjectRequest
            {
                BucketName = record.S3.Bucket.Name,
                Key = record.S3.Object.Key
            };
            
            using (var s3Client = new AmazonS3Client())
            {
                try
                {
                    var response = await s3Client.GetObjectAsync(s3Request);
                    Console.WriteLine("Object received from S3");
                    using (StreamReader reader = new StreamReader(response.ResponseStream))
                    {
                        var contents = reader.ReadToEnd();
                        emailSettings = JsonConvert.DeserializeObject<EmailSettings>(contents);
                        Console.WriteLine("JSON parsed successfully");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("The object was not received");
                    Console.WriteLine("Error message: " + ex.Message);
                }
            }

            using (var sqsClient = new AmazonSQSClient())
            {
                var attributeValues = new Dictionary<string, MessageAttributeValue>();
                attributeValues.Add("senderAddress", new MessageAttributeValue
                    { DataType = "String", StringValue = emailSettings.senderAddress });
                attributeValues.Add("receiverAddress", new MessageAttributeValue
                    { DataType = "String", StringValue = emailSettings.receiverAddress });

                var request = new SendMessageRequest(emailSettings.sqsUrl, "Message test body")
                    { MessageAttributes = attributeValues };
                try
                {
                    var responseSendMsg = await sqsClient.SendMessageAsync(request);
                    Console.WriteLine($"Message added to queue");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to add message to queue");
                    Console.WriteLine("Error message: " + ex.Message);
                }
            }
        }
    }
}