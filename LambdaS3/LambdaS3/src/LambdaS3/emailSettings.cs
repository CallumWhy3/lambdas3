namespace LambdaS3;

public class EmailSettings
{
    public string senderAddress { get; set; }
    public string receiverAddress { get; set; }

    public string sqsUrl { get; set; }
}